open System
open System.Security.Cryptography
open System.Text.RegularExpressions
open HttpClient

let sign sharedKey =
  let sharedKey = Convert.FromBase64String sharedKey
  let hasher = new HMACSHA256(sharedKey)
  fun (date : DateTime) account resource ->
    sprintf "%s\n/%s/%s" (date.ToString "R") account resource
    |> Text.Encoding.UTF8.GetBytes
    |> hasher.ComputeHash
    |> Convert.ToBase64String
    |> sprintf "SharedKeyLite %s:%s" account

let createProperty sign account table partition row property value =
  let uri = sprintf "https://%s.table.core.windows.net/%s(PartitionKey='%s',RowKey='%s')" account table partition row |> Uri
  let now = DateTime.UtcNow
  let auth = uri.AbsolutePath.Trim '/' |> sign now account
  let headers =
    [ "Authorization", auth
      "x-ms-version", "2015-04-05" ]
  let json = sprintf """{"%s" : "%O" }""" property value
  HttpClient.createRequest uri (Json json |> HttpMethod.Merge) (Some now) headers |> HttpClient.send

let createRow sign account table partition row json =
  let uri = sprintf "https://%s.table.core.windows.net/%s(PartitionKey='%s',RowKey='%s')" account table partition row |> Uri
  let now = DateTime.UtcNow
  let auth = uri.AbsolutePath.Trim '/' |> sign now account
  let headers =
    [ "Authorization", auth
      "x-ms-version", "2015-04-05" ]
  HttpClient.createRequest uri (Json json |> HttpMethod.Put) (Some now) headers |> HttpClient.send

let readRow sign account table partition row =
  let uri = sprintf "https://%s.table.core.windows.net/%s(PartitionKey='%s',RowKey='%s')" account table partition row |> Uri
  let now = DateTime.UtcNow
  let auth = uri.AbsolutePath.Trim '/' |> sign now account
  let headers =
    [ "Authorization", auth
      "x-ms-version", "2015-04-05"
      "Accept", "application/json;odata=nometadata" ]
  HttpClient.createRequest uri HttpMethod.Get (Some now) headers |> HttpClient.get

let deleteRow sign account table partition row =
  let uri = sprintf "https://%s.table.core.windows.net/%s(PartitionKey='%s',RowKey='%s')" account table partition row |> Uri
  let now = DateTime.UtcNow
  let auth = uri.AbsolutePath.Trim '/' |> sign now account
  let headers =
    [ "Authorization", auth
      "x-ms-version", "2015-04-05"
      "If-Match", "*" ]
  HttpClient.createRequest uri HttpMethod.Delete (Some now) headers |> HttpClient.send

let readProperty sign account table partition row property =
  let uri = sprintf "https://%s.table.core.windows.net/%s(PartitionKey='%s',RowKey='%s')?$select=%s" account table partition row property |> Uri
  let now = DateTime.UtcNow
  let auth = uri.AbsolutePath.Trim '/' |> sign now account
  let headers =
    [ "Authorization", auth
      "x-ms-version", "2015-04-05"
      "Accept", "application/json;odata=nometadata" ]
  async {
    let! result = HttpClient.createRequest uri HttpMethod.Get (Some now) headers |> HttpClient.get
    match result with
    | HttpClient.Success (Body json) ->
      match Regex.Match(json, sprintf """{"%s":"(.*)"}""" property).Groups |> Seq.cast<Group> |> Seq.tryItem 1 |> Option.bind (fun x -> Some x.Value) with
      | Some value -> return HttpClient.Success (Body value)
      | None -> return HttpClient.Exception (ArgumentException "property")
    | result -> return result
  }