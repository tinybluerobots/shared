[<AutoOpen>]
module ChoiceBuilder

type ChoiceBuilder() =
  member __.Zero() = Choice1Of2()
  member __.Return value = value
  member __.Bind(choice, f) =
    match choice with
    | Choice1Of2 value -> f value
    | Choice2Of2 ex -> Choice2Of2 ex
  member __.Catch f =
    try
      f() |> Choice1Of2
    with ex -> Choice2Of2 ex
  member __.Using(disposable: #System.IDisposable, body) =
    try
      let result = body disposable
      result
    finally
      disposable.Dispose()
let choice = ChoiceBuilder()