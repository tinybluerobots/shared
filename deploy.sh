. ./deploy.env

if [ "$APPNAME" = "" ] || [ "$GITURL" = "" ] || [ "$COMMENT" = "" ]; then
  echo "Environment variables not found"
else

  rm -rf deploy
  git clone $GITURL deploy

  rm -rf "deploy/$APPNAME"
  mkdir -p "deploy/$APPNAME/bin"
  cp "build/$APPNAME.exe" "deploy/$APPNAME/bin"
  cp -r "azure/" "deploy/$APPNAME"

  while read line || [[ -n "$line" ]]; do
    cp $line "deploy/$APPNAME/bin"
  done <build.files

  pushd deploy
  git add .
  git commit -m"$COMMENT"
  git push $GITURL -f
  popd
  rm -rf deploy

fi