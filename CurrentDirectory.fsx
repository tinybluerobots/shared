open System
open System.IO
open System.Reflection

let private findConfigDirectory =
  [
    #if INTERACTIVE
    Some __SOURCE_DIRECTORY__
    #else
    Some Environment.CurrentDirectory
    #endif
    (try Assembly.GetExecutingAssembly().Location |> Path.GetDirectoryName |> Some with _ -> None) ]
  |> List.choose id
  |> List.map (sprintf "%s/config.yml")
  |> List.tryFind File.Exists
  |> Option.map Path.GetDirectoryName

let getPathFor file = Path.Combine(defaultArg findConfigDirectory Environment.CurrentDirectory, file)