open System
open System.Security.Cryptography
open HttpClient

let sign sharedKey =
  let sharedKey = Convert.FromBase64String sharedKey
  let hasher = new HMACSHA256(sharedKey)
  fun (verb : HttpClient.HttpMethod) (date : DateTime) account headers resource ->
    let headers =
      headers
      |> List.sortBy fst
      |> List.map (fun (n,v) -> sprintf "%s:%s" n v)
      |> String.concat "\n"
    let signature = sprintf "%O\n\napplication/xml\n%s\n%s\n/%s/%s" verb (date.ToString "R") headers account resource
    signature
    |> Text.Encoding.UTF8.GetBytes
    |> hasher.ComputeHash
    |> Convert.ToBase64String
    |> sprintf "SharedKeyLite %s:%s" account

let authHeader auth = "Authorization", auth
let versionHeader = "x-ms-version", "2011-08-18"

let enqueue sign account queue (message : string) =
  let message = message |> Text.Encoding.UTF8.GetBytes |> Convert.ToBase64String |> sprintf "<QueueMessage><MessageText>%s</MessageText></QueueMessage>"
  let uri = sprintf "https://%s.queue.core.windows.net/%s/messages" account queue |> Uri
  let now = DateTime.UtcNow
  let resource = sprintf "%s/messages" queue
  let headers = [ versionHeader ]
  let meth = Xml message |> HttpMethod.Post
  let auth = sign meth now account headers resource |> authHeader
  let headers = auth :: headers
  HttpClient.createRequest uri meth (Some now) headers
  |> HttpClient.send