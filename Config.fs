module Config

open System
open System.IO
open FSharp.Configuration

[<Literal>]
let configFile = "config.yml"
type Config = YamlConfig<configFile>

let config = Config()

let assemblyLocation = Reflection.Assembly.GetExecutingAssembly().Location

let sourceDirectory = 
  assemblyLocation
  |> Path.GetFileNameWithoutExtension
  |> sprintf "D:\\home\\site\\wwwroot\\%s"
  |> fun configPath -> Directory.Exists configPath, configPath
  |> function
  | true, configPath -> configPath
  | _ -> Path.GetDirectoryName assemblyLocation

let sourceFile fileName = Path.Combine(sourceDirectory, fileName)

Path.Combine(sourceDirectory, configFile)
|> config.Load