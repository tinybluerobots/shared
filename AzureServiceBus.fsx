open System
open System.Security.Cryptography
open HttpClient
open System.Net
open System.Text

let sign keyName (key : string) =
  let hasher = new HMACSHA256(Encoding.UTF8.GetBytes key)
  fun resource ->
    let epoch = DateTime.UtcNow - DateTime(1970, 1, 1)
    let expiry = epoch.TotalSeconds |> int64 |> (+) 600L |> string
    let resource = WebUtility.UrlEncode resource
    let signature =
      sprintf "%s\n%s" resource expiry
      |> Encoding.UTF8.GetBytes
      |> hasher.ComputeHash
      |> Convert.ToBase64String
      |> WebUtility.UrlEncode
    sprintf "SharedAccessSignature sig=%s&se=%s&skn=%s&sr=%s" signature expiry keyName resource

let accountUri account = sprintf "https://%s.servicebus.windows.net" account
let resourceUrl account resource = (accountUri account, resource) ||> sprintf "%s/%s"
let authHeader auth = "Authorization", auth

let brokerProperties (message: string) =
  "BrokerProperties",
  message
  |> Encoding.UTF8.GetBytes
  |> Convert.ToBase64String
  |> Seq.truncate 128
  |> fun x -> String.Join("", x)
  |> sprintf """{"MessageId":"%s"}"""

let enqueue sign account resource message =
  let uri = resourceUrl account resource
  let headers = [ sign uri |> authHeader ]
  let headers =
    match String.IsNullOrWhiteSpace message with
    | true -> headers
    | _ -> brokerProperties message :: headers
  let uri = sprintf "%s/messages" uri
  HttpClient.createRequest (Uri uri) (Json message |> HttpMethod.Post) None headers |> HttpClient.send

let enqueueDelayed sign account resource message (time : DateTime) =
  let uri = resourceUrl account resource
  let headers = ("BrokerProperties", time.ToString "O" |> sprintf "{\"ScheduledEnqueueTimeUtc\": \"%s\" }") :: [ sign uri |> authHeader ]
  let uri = sprintf "%s/messages" uri
  HttpClient.createRequest (Uri uri) (Json message |> HttpMethod.Post) None headers |> HttpClient.send

let enqueueBatch sign account resource messages =
  let uri = resourceUrl account resource
  let headers = [ sign uri |> authHeader ]
  let uri = sprintf "%s/messages" uri
  let messages =
    messages
    |> Seq.map (sprintf """{"Body":"%s"}""")
    |> String.concat ","
    |> sprintf "[%s]"
  let content = Custom("application/vnd.microsoft.servicebus.json", messages)
  HttpClient.createRequest (Uri uri) (HttpMethod.Post content) None headers |> HttpClient.send

let subscription sign account topic subscription =
  let uri = accountUri account
  let headers = [ sign uri |> authHeader ]
  let uri = sprintf "%s/%s/Subscriptions/%s" uri topic subscription
  HttpClient.createRequest (Uri uri) HttpMethod.Get None headers |> HttpClient.get

let receiveSubscription sign account topic subscription =
  let uri = resourceUrl account topic
  let headers = [ sign uri |> authHeader ]
  let uri = sprintf "%s/subscriptions/%s/messages/head" uri subscription
  HttpClient.createRequest (Uri uri) HttpMethod.Delete None headers |> HttpClient.get

let receiveQueue sign account queue =
  let uri = resourceUrl account queue
  let headers = [ sign uri |> authHeader ]
  let uri = sprintf "%s/messages/head" uri
  HttpClient.createRequest (Uri uri) HttpMethod.Delete None headers |> HttpClient.get

let peekSubscription sign account topic subscription =
  let uri = resourceUrl account topic
  let headers = [ sign uri |> authHeader ]
  let uri = sprintf "%s/subscriptions/%s/messages/head" uri subscription
  HttpClient.createRequest (Uri uri) (HttpMethod.Post NoContent) None headers |> HttpClient.get

let peekQueue sign account queue =
  let uri = resourceUrl account queue
  let headers = [ sign uri |> authHeader ]
  let uri = sprintf "%s/messages/head" uri
  HttpClient.createRequest (Uri uri) (HttpMethod.Post NoContent) None headers |> HttpClient.get

let listTopics sign account =
  let uri = sprintf "https://%s.servicebus.windows.net" account
  let headers = [ sign uri |> authHeader ]
  let uri = sprintf "%s/$Resources/Topics" uri
  HttpClient.createRequest (Uri uri) HttpMethod.Get None headers |> HttpClient.get