module Log

#nowarn "52"
open System
open System.Net

type private Level = Debug | Info | Warn | Error | Fatal

let mutable private consoleEnabled = false

[<NoComparison>]
type private Message = Send | SendAndWait of AsyncReplyChannel<unit> | Log of string | SetUrl of string

let private getHost() =
  [ "COMPUTERNAME" ; "HOSTNAME" ; "USERNAME" ; "USER" ]
  |> Seq.map Environment.GetEnvironmentVariable
  |> Seq.find (isNull >> not)

let private logMessageSenderMbx (mbx: MailboxProcessor<Message>) =
  let messages = ResizeArray<string>()
  let send (url: string) =
    let body = messages |> String.concat "\n"
    use wc = new WebClient()
    wc.UploadStringTaskAsync(url, body)
    |> Async.AwaitTask
    |> Async.Catch
    |> Async.RunSynchronously
    |> function
    | Choice1Of2 _ -> messages.Clear()
    | Choice2Of2 ex -> sprintf "Log failed with exception %O" ex |> stderr.WriteLine
  let rec loop url =
    async {
      let! msg = mbx.Receive()
      match msg, url with
      | SetUrl url, _ -> return! Some url |> loop
      | Log log, Some _ -> messages.Add log
      | Send, Some url when messages.Count > 0 -> send url
      | SendAndWait reply, Some url ->
        while messages.Count > 0 do send url
        reply.Reply()
      | SendAndWait reply, None ->
        reply.Reply()
      | _ -> ()
      return! loop url
    }
  loop None

let private logMessageSender = MailboxProcessor.Start logMessageSenderMbx

let private post (level : Level) text =
  if String.IsNullOrWhiteSpace text |> not then
    let background, foreground =
      match level with
      | Debug -> ConsoleColor.Black, ConsoleColor.DarkGray
      | Info -> ConsoleColor.Black, ConsoleColor.White
      | Warn -> ConsoleColor.Black, ConsoleColor.Yellow
      | Error -> ConsoleColor.Black, ConsoleColor.Red
      | Fatal -> ConsoleColor.DarkRed, ConsoleColor.White
    Console.BackgroundColor <- background
    Console.ForegroundColor <- foreground
    let now = DateTime.UtcNow.ToString "O"
    let level = (sprintf "%+A" level).ToLower()
    let msg = sprintf "%s [%s] %s" now level text
    Log msg |> logMessageSender.Post

    if consoleEnabled && not Console.IsOutputRedirected then printfn "\r\n%s\r\n" msg
    Console.ResetColor()

let private format (text: string) = text.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ")

let private logf level fmt = Printf.ksprintf (format >> post level) fmt
let debugf fmt = logf Debug fmt
let infof fmt = logf Info fmt
let warnf fmt = logf Warn fmt
let errorf fmt = logf Error fmt
let fatalf fmt = logf Fatal fmt

let debug o = debugf "%O" o
let info o = infof "%O" o
let warn o = warnf "%O" o
let error o = errorf "%O" o
let fatal o = fatalf "%O" o
let raise ex =
  fatal ex
  raise ex

let initialize name token =
  let host = getHost()
  let url = sprintf "https://www.scalyr.com/api/uploadLogs?token=%s&host=%s&logfile=%s&parser=default" token host name
  let timer = new Timers.Timer(1000.)
  if (String.IsNullOrWhiteSpace >> not) token then
    SetUrl url |> logMessageSender.Post
    timer.Elapsed.Add(fun _ -> logMessageSender.Post Send)
    timer.Start()
  fun () ->
    timer.Stop()
    timer.Dispose()
    logMessageSender.PostAndReply(SendAndWait, 30000)
    (logMessageSender :> IDisposable).Dispose()

let enableConsole() = consoleEnabled <- true