open System

type Next = Day | Hour | HalfHour | QuarterHour | Minute

let until next f =
  let now = DateTime.UtcNow
  let toNearest i (now : DateTime) =  i - (now.Minute - (now.Minute / i * i)) |> float |> TimeSpan.FromMinutes
  let timeSpan =
    match next with
    | Day -> DateTime.Today.AddDays 1. - now
    | Hour -> 60 - now.Minute |> float |> TimeSpan.FromMinutes
    | HalfHour -> now |> toNearest 30
    | QuarterHour -> now |> toNearest 15
    | Minute -> 60 - now.Second |> float |> TimeSpan.FromSeconds
  now + timeSpan |> sprintf "Waiting until %O" |> f
  timeSpan
  |> fun t -> t.TotalMilliseconds
  |> int
  |> Async.Sleep
  |> Async.RunSynchronously