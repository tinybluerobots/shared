open System
open System.Security.Cryptography
open HttpClient

let sign sharedKey =
  let sharedKey = Convert.FromBase64String sharedKey
  let hasher = new HMACSHA256(sharedKey)
  fun (verb : HttpClient.HttpMethod) (date : DateTime) account headers contentType resource ->
    let contentType = defaultArg contentType ""
    let headers =
      headers
      |> List.sortBy fst
      |> List.map (fun (n,v) -> sprintf "%s:%s" n v)
      |> String.concat "\n"
    let signature = sprintf "%O\n\n%s\n%s\n%s\n/%s/%s" verb contentType (date.ToString "R") headers account resource
    signature
    |> Text.Encoding.UTF8.GetBytes
    |> hasher.ComputeHash
    |> Convert.ToBase64String
    |> sprintf "SharedKeyLite %s:%s" account

let authHeader auth = "Authorization", auth
let versionHeader = "x-ms-version", "2015-12-11"

let private createRequest sign account container id meth headers contentType =
  let uri = sprintf "https://%s.blob.core.windows.net/%s/%s" account container id |> Uri
  let now = DateTime.UtcNow
  let headers = versionHeader :: headers
  let auth = uri.AbsolutePath.Trim '/' |> sign meth now account headers contentType
  let headers = authHeader auth :: headers
  HttpClient.createRequest uri meth (Some now) headers

let createBlob sign account container id json =
  createRequest sign account container id (Json json |> HttpMethod.Put) ["x-ms-blob-type", "BlockBlob"] (Some "application/json")
  |> HttpClient.send

let deleteBlob sign account container id =
  createRequest sign account container id HttpMethod.Delete [] None
  |> HttpClient.send

let readBlob sign account container id =
  createRequest sign account container id HttpMethod.Get [] None
  |> HttpClient.get

let append name opt s =
  match opt with
  | Some value -> sprintf "%s&%s=%O" s name value
  | _ -> s

let listBlobs sign account container (prefix : string option) (maxResults : int option) (marker : string option) =
  let uri =
    sprintf "https://%s.blob.core.windows.net/%s?restype=container&comp=list&include=metadata" account container
    |> append "maxresults" maxResults
    |> append "marker" marker
    |> append "prefix" prefix
    |> Uri
  let now = DateTime.UtcNow
  let resource = sprintf "%s?comp=list" container
  let headers = [ versionHeader ]
  let auth = sign HttpMethod.Get now account headers None resource
  let headers = authHeader auth :: headers
  HttpClient.createRequest uri HttpMethod.Get (Some now) headers
  |> HttpClient.get

let createMetadata sign account container id metadata =
  let uri =
    sprintf "https://%s.blob.core.windows.net/%s/%s?comp=metadata" account container id
    |> Uri
  let now = DateTime.UtcNow
  let resource = sprintf "%s/%s?comp=metadata" container id
  let headers = versionHeader :: List.map (fun (n,v) -> sprintf "x-ms-meta-%s" n, v) metadata
  let auth = sign (HttpMethod.Put NoContent) now account headers None resource |> authHeader
  let headers = auth :: headers
  HttpClient.createRequest uri (HttpMethod.Put NoContent) (Some now) headers
  |> HttpClient.send

let readMetadata sign account container id =
  let uri =
    sprintf "https://%s.blob.core.windows.net/%s/%s" account container id
    |> Uri
  let now = DateTime.UtcNow
  let resource = sprintf "%s/%s" container id
  let headers = [ versionHeader ]
  let auth = sign HttpMethod.Head now account headers None resource |> authHeader
  let headers = auth :: headers
  HttpClient.createRequest uri HttpMethod.Head (Some now) headers
  |> HttpClient.getHeaders