eval `docker-machine env tbrdocker` && export DOCKER_API_VERSION=1.23
docker rm -f `docker ps -qa -f ancestor=$1`
docker build -t $1 build
docker run -d -h $1 --name $1 --restart=always $1
docker images | grep "<none>" | awk "{print \$3}" | xargs docker rmi