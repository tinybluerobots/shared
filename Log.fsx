open System

type private Level = Debug | Info | Error | Fatal

let hostName =
  match Environment.GetEnvironmentVariable "HOSTNAME" with
  | null -> Environment.GetEnvironmentVariable "USER"
  | value -> value

let private logf (level : Level) formatter o =
  let now = DateTime.UtcNow.ToString "O"
  let level = (sprintf "%+A" level).ToLower()
  sprintf formatter o |> sprintf "%s %s [%s] %s" now hostName level |> printfn "%s"

let errorf formatter o = logf Error formatter o
let infof formatter o = logf Info formatter o
let debugf formatter o = logf Debug formatter o
let fatalf formatter o = logf Fatal formatter o
let info o = infof "%O" o
let error o = errorf "%O" o
let debug o = debugf "%O" o
let fatal o = fatalf "%O" o

info "Log started"