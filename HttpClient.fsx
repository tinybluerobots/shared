open System
open System.Net
open System.Net.Http

[<NoComparison>]
type Success =
  | Empty
  | Body of string
  | Headers of Map<string, string>

[<NoComparison>]
type Result =
  | Success of Success
  | Failure of int * string option
  | Exception of exn

type Content =
  | Custom of Type : string * Content : string
  | Json of string
  | Text of string
  | Xml of string
  | NoContent

type HttpMethod =
  | Put of Content
  | Get
  | Delete
  | Merge of Content
  | Post of Content
  | Head
  with
  override this.ToString() =
    let s = (sprintf "%A" this).ToUpper()
    match s.Contains " " with
    | true -> s.Substring(0, s.IndexOf ' ')
    | _ -> s
    |> fun x -> x.Trim '\n'

let httpClient = new HttpClient()
httpClient.Timeout <- TimeSpan.FromSeconds 30.

let createRequest uri httpMethod (date : DateTime option) headers =
  let meth, content =
    match httpMethod with
    | Put content -> System.Net.Http.HttpMethod.Put, content
    | Post content -> System.Net.Http.HttpMethod.Post, content
    | Get -> System.Net.Http.HttpMethod.Get, NoContent
    | Delete -> System.Net.Http.HttpMethod.Delete, NoContent
    | Merge content -> System.Net.Http.HttpMethod "MERGE", content
    | Head -> System.Net.Http.HttpMethod.Head, NoContent
  let httpRequestMessage = new HttpRequestMessage()
  match content with
  | Json content ->
      httpRequestMessage.Content <- new StringContent(content)
      httpRequestMessage.Content.Headers.ContentType <- new Headers.MediaTypeHeaderValue("application/json")
  | Text content ->
    httpRequestMessage.Content <- new StringContent(content)
  | Xml content ->
      httpRequestMessage.Content <- new StringContent(content)
      httpRequestMessage.Content.Headers.ContentType <- new Headers.MediaTypeHeaderValue("application/xml")
  | Custom(contentType, content) ->
      httpRequestMessage.Content <- new StringContent(content)
      httpRequestMessage.Content.Headers.ContentType <- new Headers.MediaTypeHeaderValue(contentType)
  | _ -> ()
  httpRequestMessage.RequestUri <- uri
  httpRequestMessage.Method <- meth
  let addHeader (n : string, v : string) = httpRequestMessage.Headers.Add(n,v)
  if date.IsSome then httpRequestMessage.Headers.Date <- Nullable<DateTimeOffset>(DateTimeOffset date.Value)
  headers |> List.iter addHeader
  httpRequestMessage

let private getBody (httpResponseMessage : HttpResponseMessage) =
  async {
    let! content = httpResponseMessage.Content.ReadAsStringAsync() |> Async.AwaitTask |> Async.Catch
    let result =
      match content, httpResponseMessage.StatusCode with
      | Choice1Of2 body, HttpStatusCode.OK -> Body body |> Success
      | Choice1Of2 body, statusCode -> Failure(int statusCode, Some body |> Option.filter (String.IsNullOrWhiteSpace >> not))
      | Choice2Of2 ex, _ -> Exception ex
    return result
  }

let private getResponse httpRequestMessage =
  async {
    return! httpClient.SendAsync httpRequestMessage
    |> Async.AwaitTask
    |> Async.Catch
  }

let send httpRequestMessage =
  async {
    let! response = getResponse httpRequestMessage
    let! result =
      match response with
      | Choice2Of2 ex -> async { return Exception ex }
      | Choice1Of2 response ->
        let result =
          match response.StatusCode with
          | HttpStatusCode.Created -> async { return Success Empty }
          | HttpStatusCode.NoContent -> async { return Success Empty }
          | HttpStatusCode.Accepted -> async { return Success Empty }
          | HttpStatusCode.OK -> async { return Success Empty }
          | _ -> async { return! getBody response }
        result
    return result
  }

let get httpRequestMessage =
  async {
    let! response = getResponse httpRequestMessage
    let! result =
      match response with
      | Choice1Of2 response ->
        async {
          let! result = getBody response
          return result }
      | Choice2Of2 ex -> async { return Exception ex }
    return result
  }

let getHeaders httpRequestMessage =
  async {
    let! response = getResponse httpRequestMessage
    let result =
      match response with
      | Choice1Of2 response ->
        let result =
          response.Headers
          |> Seq.map (fun x -> x.Key, Seq.head x.Value)
          |> Map.ofSeq
          |> Headers
          |> Success
        result
      | Choice2Of2 ex -> Exception ex
    return result
  }

let exitOnError (log : obj -> unit) =
  function
  | Failure (statusCode, msg) ->
    let msg =
      match msg with
      | Some msg -> sprintf "Http Error %i : %s" statusCode msg
      | None -> sprintf "Http Error %i" statusCode
    log msg
    failwith msg
  | Exception ex ->
    log ex
    stderr.WriteLine ex
    raise ex
  | _ -> failwith "Success must be handled"