open System
open HttpFs.Client
open HttpFs
open Hopac

let jsonHeader = ContentType.create("application", "json") |> ContentType

let private send apiKey name index json =
  sprintf "https://%s.search.windows.net/indexes/%s/docs/index?api-version=2016-09-01" name index
  |> Request.createUrl Post
  |> Request.setHeader jsonHeader
  |> Request.setHeader (Custom ("api-key", apiKey))
  |> Request.responseAsString
  |> Job.catch

let deleteDocument apiKey name index id =
  let json = sprintf """{"value":[{"@search.action":"delete","id":"%s"}]}""" id
  send apiKey name index json

let createDocument apiKey name index id kvps =
  let json = sprintf """{"value":[{"@search.action":"mergeOrUpload","id":"%s",%s}]}""" id kvps
  send apiKey name index json

let search apiKey name index query =
  sprintf "https://%s.search.windows.net/indexes/%s/docs?api-version=2016-09-01&$count=true&%s" name index query
  |> Request.createUrl Get
  |> Request.setHeader (Custom ("api-key", apiKey))
  |> Request.responseAsString
  |> Job.catch

let readDocument apiKey name index id =
  sprintf "https://%s.search.windows.net/indexes/%s/docs/%s?api-version=2016-09-01" name index id
  |> Request.createUrl Get
  |> Request.setHeader (Custom ("api-key", apiKey))
  |> Request.responseAsString
  |> Job.catch